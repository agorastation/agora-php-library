<?php
namespace Agora\Library\Factory;

/**
 * 簡易インスタンスキャッシュトレイト
 */
trait SimpleInstanceCachingTrait
{

    private static $instanceCaches = [];

    /**
     * インスタンスを生成する
     * @param string $className クラス完全修飾名
     * @param string|null $factoryName ファクトリメソッド名(nullでコンストラクタ利用)
     * @param ...mixed ...$args ファクトリ(コンストラクタ)引数
     * @return mixed
     */
    protected function createInstance (
        string $className, ?string $factoryName, ...$args)
    {
        // 初回のクラス名指定であればキーを作成
        if (!array_key_exists($className, self::$instanceCaches)) {
            self::$instanceCaches[$className] = [];
        }

        // 引数リストが一致するキャッシュされたインスタンスを返す
        foreach (self::$instanceCaches[$className] as $item) {
            if ($item['args'] == $args) return $item['instance'];
        }

        // インスタンスが未生成であれば生成して返却
        $instance = is_null($factoryName)
            ? new $className(...$args)
            : $className::{$factoryName}(...$args);
        self::$instanceCaches[$className][] = ['instance' => $instance, 'args' => $args];
        return $instance;
    }

}
