<?php
namespace Tests\Classes\Agora\Library\Data;

use Agora\Library\Data;

/**
 * テストDTO
 * @property int $id ID
 * @property string $name 名称
 * @property-read string $csv カンマ区切りデータ
 */
class TestDto
    extends Data\DtoAbstract
{

    protected $id = 0;

    protected $name = '';

    public function setName(string $name)
    {
        $this->name = trim($name);
        return $this;
    }

    public function getCsv(): string
    {
        return implode(',', [$this->id, $this->name]);
    }

}
