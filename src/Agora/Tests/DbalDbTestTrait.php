<?php
namespace Agora\Tests;

use Doctrine\DBAL;
use Symfony\Component\Yaml\Yaml;

/**
 * DBAL-DBユニットテストトレイト
 */
trait DbalDbTestTrait
{

    /**
     * DBアダプタを生成する
     * @param array $params
     *  {
     *      'driver': {ドライバ:string},
     *      'host': {ホスト名:string},
     *      'port': {ポート番号:int},
     *      'dbname': {DB名:string},
     *      'user': {DBユーザ:string},
     *      'password': {パスワード:string},
     *  }
     * @return DBAL\Driver\Connection
     */
    protected static function createDbAdapter(array $params = []): DBAL\Driver\Connection
    {
        static $adapter = null;
        return is_null($adapter)
            ? ($adapter = DBAL\DriverManager::getConnection($params))
            : $adapter;
    }

    /**
     * DBの初期化
     */
    protected static function initializeDb(
        DBAL\Driver\Connection $adapter, string $path)
    {
        $directoryIterator = new \RecursiveDirectoryIterator(
            $path,
            \FileSystemIterator::CURRENT_AS_PATHNAME
            | \FileSystemIterator::SKIP_DOTS);

        $iterator = new \RecursiveIteratorIterator(
            $directoryIterator,
            \RecursiveIteratorIterator::LEAVES_ONLY);

        $pathList = iterator_to_array($iterator, false);
        sort($pathList);
        foreach ($pathList as $path) {
            // ファイル以外はスキップ
            if (!is_file($path)) continue;

            // 拡張子取得
            $extension = pathinfo($path, PATHINFO_EXTENSION);

            // .ymlの実行
            if ($extension == 'yml') self::initializeTables($adapter, $path);

            // .sqlの実行
            if ($extension == 'sql') self::runQuery($adapter, $path);
        }
    }

    /**
     * テーブルを初期化する
     * @param string $ymlPath yamlファイルパス
     * @return void
     */
    private static function initializeTables(
        DBAL\Driver\Connection $adapter, string $ymlPath)
    {
        $ymlData = Yaml::parseFile($ymlPath);
        foreach ($ymlData as $table => $rows) {
            $adapter->query("delete from {$table}");
            foreach ($rows as $row) {
                $adapter->insert($table, $row);
            }
        }
    }

    /**
     * クエリを実行する
     * @static
     * @param string $sqlFilePath SQLファイルパス
     * @return void
     */
    private static function runQuery(
        DBAL\Driver\Connection $adapter, string $sqlFilePath)
    {
        $sql = file_get_contents($sqlFilePath);
        foreach (preg_split('/;/s', $sql, null, PREG_SPLIT_NO_EMPTY) as $statement) {
            $statement = trim($statement);
            if (!empty($statement)) {
                $adapter->query($statement);
            }
        }
    }

}
