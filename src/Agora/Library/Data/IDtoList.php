<?php
namespace Agora\Library\Data;

/**
 * DTOリストインターフェース
 */
interface IDtoList
    extends \IteratorAggregate
{

    /** @return int リスト件数 */
    public function getCount(): int;

}
