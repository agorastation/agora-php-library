<?php
namespace Agora\Library\Adapter\Data;

use Agora\Library\Data;
use Doctrine\DBAL;

/**
 * DBALステートメントDTOリスト
 */
class DbalStatementDtoList
    implements Data\IDtoList
{

    /** @var DBAL\Driver\Statement DBALステートメント */
    private $statement = null;

    /** @var \ArrayIterator|null キャッシュされたリスト */
    private $cachedList = null;

    /** @var mixed[] fetch設定 */
    private $fetchArguments = [];

    /**
     * constructor
     * @param DBAL\Driver\Statement $statement DBALステートメント
     * @param string $dtoName DTOクラス完全修飾名
     * @return void
     */
    public function __construct(DBAL\Driver\Statement $statement, string $dtoName)
    {
        $this->fetchArguments = [DBAL\FetchMode::CUSTOM_OBJECT, $dtoName, [[], true]];
        $this->statement = $statement;
    }

    /**
     * @inheritdoc
     */
    public function getCount(): int
    {
        return count($this->cache()->cachedList);
    }

    /**
     * @inheritdoc
     */
    public function getIterator(): \Traversable
    {
        return $this->cache()->cachedList;
    }

    /**
     * 結果リストのキャッシュ
     * @return DbalStatementDtoList
     */
    private function cache(): DbalStatementDtoList
    {
        if (is_null($this->cachedList)) {
            $list = $this->statement->fetchAll(...$this->fetchArguments);
            $this->cachedList = new \ArrayIterator($list);
        }
        return $this;
    }

}
