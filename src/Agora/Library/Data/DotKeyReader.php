<?php
namespace Agora\Library\Data;

/**
 * ドット区切りキーによる配列参照リーダ
 */
class DotKeyReader
    implements \ArrayAccess
{

    /** @var array 対象配列 */
    private $values = [];

    /** @var bool safetyモード */
    private $isSafety = false;

    /**
     * constructor
     * @param array $values OPTIONAL 対象配列
     * @param bool $isSafety OPTIONAL
     * @return void
     */
    public function __construct(array $values = [], bool $isSafety = false)
    {
        $this->setValues($values)
            ->setIsSafety($isSafety);
    }

    /**
     * クラスの関数コール
     * @inheritdoc self::get()
     */
    public function __invoke(string $key = null, $default = null)
    {
        return $this->get($key, $default);
    }

    /**
     * ドット区切りのキーから連想配列へのデコード
     * @param array $dotKeyList ドット区切りキーのリスト
     * @return array
     */
    public static function decode(array $dotKeyList): array
    {
        $decoded = [];
        foreach ($dotKeyList as $dotKey => $value) {
            $node = &$decoded;
            foreach (explode('.', $dotKey) as $key) {
                if (!array_key_exists($key, $node)) $node[$key] = [];
                $node = &$node[$key];
            }
            $node = $value;
        }
        return $decoded;
    }

    /**
     * 値を取得する
     * @param string $key OPTIONAL キー
     * @param mixed $default OPTIONAL 既定値(safetyモード専用)
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public function get(string $key = null, $default = null)
    {
        $value = $this->values;
        if (is_null($key)) return $value;

        foreach (explode('.', $key) as $node) {
            if (!array_key_exists($node, $value)) {
                if ($this->isSafety) {
                    return $default;
                } else {
                    throw new \InvalidArgumentException("undefined key {$key}");
                }
            }
            $value = $value[$node];
        }
        return $value;
    }

    /**
     * @inheritdoc
     */
    public function offsetExists($offset): bool
    {
        $isSafety = $this->isSafety;

        try {
            $this->setIsSafety(false)->get($offset);
        } catch (\InvalidArgumentException $e) {
            $e;
            $this->setIsSafety($isSafety);
            return false;
        }
        $this->setIsSafety($isSafety);
        return true;
    }

    /**
     * @inheritdoc
     */
    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    /**
     * @inheritdoc
     */
    public function offsetSet($offset, $value): void
    {
        throw new \DomainException('not supported');
    }

    /**
     * @inheritdoc
     */
    public function offsetUnset($offset): void
    {
        throw new \DomainException('not supported');
    }

    /**
     * 対象配列を設定する
     * @param array $values 対象配列
     * @return static
     */
    protected function setValues(array $values)
    {
        $this->values = $values;
        return $this;
    }

    /**
     * safetyモードを設定する
     * @param bool $isSafety safetyモード
     * @return static
     */
    protected function setIsSafety(bool $isSafety)
    {
        $this->isSafety = $isSafety;
        return $this;
    }

}
