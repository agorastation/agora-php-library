<?php
namespace Tests\Cases\Agora\Library\Factory;

use Tests\Classes\Agora\Library\Factory;
use Tests\Classes\Agora\Library\Data\TestDto;

/**
 * シングルトンファクトリトレイト ユニットテスト
 * @coversDefaultClass \Agora\Library\Factory\SimpleInstanceCachingTrait
 * @see \Agora\Library\Factory\SimpleInstanceCachingTrait
 */
final class SimpleInstanceCachingTraitTest
    extends \Tests\Cases\TestAbstract
{

    public function setUp(): void
    {
        parent::setUp();
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @group createInstance
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_createInstanceWithCtor()
    {
        /* Arrange */
        $fieldsA = ['id' => 3, 'name' => 'test3'];
        $fieldsB = ['id' => 4, 'name' => 'test4'];

        $sut = new Factory\TestFactoryA();

        /* Act */
        /* Assert */
        $this->assertSame(
            $sut->create(TestDto::class, null, $fieldsA),
            $sut->create(TestDto::class, null, $fieldsA));
        $this->assertNotEquals(
            $sut->create(TestDto::class, null, $fieldsA),
            $sut->create(TestDto::class, null, $fieldsB));

        $dto = $sut->create(TestDto::class, null, $fieldsA);
        $this->assertSame($fieldsA['id'], $dto->id);
        $this->assertSame($fieldsA['name'], $dto->name);
    }

    /**
     * @group createInstance
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_createInstanceWithFactoryMethod()
    {
        /* Arrange */
        $fieldsA = ['id' => 3, 'name' => 'test3'];
        $fieldsB = ['id' => 4, 'name' => 'test4'];

        $sut = new Factory\TestFactoryA();

        /* Act */
        /* Assert */
        $this->assertSame(
            $sut->create(TestDto::class, 'create', $fieldsA),
            $sut->create(TestDto::class, 'create', $fieldsA));
        $this->assertNotEquals(
            $sut->create(TestDto::class, 'create', $fieldsA),
            $sut->create(TestDto::class, 'create', $fieldsB));

        $dto = $sut->create(TestDto::class, 'create', $fieldsA);
        $this->assertSame($fieldsA['id'], $dto->id);
        $this->assertSame($fieldsA['name'], $dto->name);
    }

    /**
     * @group sameInstance
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_sameInstance()
    {
        /* Arrange */
        $fields = ['id' => 3, 'name' => 'test3'];

        $factoryA = new Factory\TestFactoryA();
        $factoryB = new Factory\TestFactoryB();

        /* Act */
        /* Assert */
        $this->assertSame(
            $factoryA->create(TestDto::class, 'create', $fields),
            $factoryB->create(TestDto::class, 'create', $fields));
    }

}
