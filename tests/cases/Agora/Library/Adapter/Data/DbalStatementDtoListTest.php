<?php
namespace Tests\Cases\Agora\Library\Adapter\Data;

use Agora\Library\Adapter\Data;
use Tests\Classes\Agora\Library\Data\TestDto;

/**
 * DBALステートメントDTOリスト ユニットテスト
 * @coversDefaultClass \Agora\Library\Adapter\Data\DbalStatementDtoList
 * @see Data\DbalStatementDtoList
 */
final class DbalStatementDtoListTest
    extends \Tests\Cases\DbalDbTestAbstract
{

    public function setUp(): void
    {
        parent::setUp();
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @group getIterator
     * @group getIteratorReal
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_getIterator()
    {
        /* Arrange */
        $dtoName = TestDto::class;
        $expect = [
            'list' => [
                ['id' => 1, 'name' => 'テスト1'],
                ['id' => 2, 'name' => 'テスト2'],
                ['id' => 3, 'name' => 'テスト3'],
                ['id' => 4, 'name' => 'テスト4'],
                ['id' => 5, 'name' => 'テスト5'],
                ['id' => 6, 'name' => 'テスト6'],
                ['id' => 7, 'name' => 'テスト7'],
                ['id' => 8, 'name' => 'テスト8'],
            ]
        ];

        $statement = self::$adapter->createQueryBuilder()
            ->from('t_test')
            ->select(['id', 'name'])
            ->execute();

        $list = new Data\DbalStatementDtoList($statement, $dtoName);

        /* Act */
        /* Assert */
        foreach ($list as $index => $item) {
            $this->assertEquals(TestDto::create($expect['list'][$index]), $item);
        }
    }

    /**
     * @group getCount
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_getCount()
    {
        /* Arrange */
        $statement = self::$adapter->createQueryBuilder()
            ->from('t_test')
            ->select(['id', 'name'])
            ->where('id < :id')->setParameter('id', 6)
            ->execute();

        $list = new Data\DbalStatementDtoList($statement, TestDto::class);

        /* Act */
        $count = $list->getCount();

        /* Assert */
        $this->assertSame(5, $count);
    }

}
