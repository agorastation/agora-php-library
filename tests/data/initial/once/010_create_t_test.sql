create table if not exists t_test (
    id integer not null primary key,
    name text not null,
    created_at datetime not null default(datetime('now', 'localtime')),
    modified_at datetime not null default(datetime('now', 'localtime'))
);
