<?php
/**
 * 抽象DBAL-DBテスト
 */
namespace Tests\Cases;

use Doctrine\DBAL;

class DbalDbTestAbstract
    extends TestAbstract
{

    /** Traits */
    use
        // DBAL-DBユニットテストトレイト
        \Agora\Tests\DbalDbTestTrait;

    /** @var DBAL\Connection */
    protected static $adapter = null;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        self::$adapter = self
            ::createDbAdapter(['driver' => 'pdo_sqlite', 'dbname' => ':memory:']);

        self::initializeDb(self::$adapter, PROJECT_ROOT . "tests/data/initial/once/");
    }

    public function setUp(): void
    {
        parent::setUp();

        self::initializeDb(self::$adapter, "{$this->projectRoot}tests/data/initial/each/");
    }

}
