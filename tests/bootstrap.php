<?php
(function() {
    $projectRoot = realpath(__dir__ . '/..') . '/';
    define('PROJECT_ROOT', $projectRoot);

    require "{$projectRoot}vendor/autoload.php";

})();
