<?php
namespace Agora\Library\Data;

/**
 * DTOページングリストインターフェース
 */
interface IPager
    extends \IteratorAggregate
{

    /** @return int リスト総件数 */
    public function getCount(): int;

    /** @return int[] ページ番号リスト */
    public function getPageNumbers(): array;

    /** @return int ページ数 */
    public function getPageCount(): int;

    /** @return int ページ毎の件数 */
    public function getCountPerPage(): int;

    /** @return int 現在のページ番号 */
    public function getCurrentPageNumber(): int;

    /** @return iterable 現在のページのリスト */
    public function getCurrentPage(): IDtoList;

    /**
     * ページ毎の件数を設定する
     * @param int $count ページ毎の件数
     * @return IPager
     */
    public function setCountPerPage(int $count): IPager;

    /**
     * 現在のページ番号を設定する
     * @param int $number ページ番号
     * @return IPager
     */
    public function setCurrentPageNumber(int $number): IPager;

}
