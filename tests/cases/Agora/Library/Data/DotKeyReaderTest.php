<?php
namespace Tests\Cases\Agora\Library\Data;

use Agora\Library\Data;
use Cake\Chronos\Chronos;

/**
 * ドット区切りキーによる配列参照リーダ ユニットテスト
 * @coversDefaultClass \Agora\Library\Data\DotKeyReader
 * @see Data\DotKeyReader
 */
final class DotKeyReaderTest
    extends \Tests\Cases\TestAbstract
{

    public function setUp(): void
    {
        parent::setUp();
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    public function getDataProvider()
    {
        $values = $this->getValues();

        yield 'leaf' => [
            'values' => $values,
            'key' => 'jwt.secret.entryAccountActivation',
            'expect' => ['value' => 'test_secret'],
        ];

        yield 'node' => [
            'values' => $values,
            'key' => 'jwt',
            'expect' => [
                'value' => [
                    'secret' => [
                        'entryAccountActivation' => 'test_secret',
                    ],
                    'algorithm' => 'HS512',
                ],
            ],
        ];

        yield 'all' => [
            'values' => $values,
            'key' => null,
            'expect' => [
                'value' => $values,
            ],
        ];

        yield 'object value' => [
            'values' => $values,
            'key' => 'some',
            'expect' => [
                'value' => $values['some'],
            ],
        ];
    }

    /**
     * @dataProvider getDataProvider
     * @group get
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_get(array $values, ?string $key, array $expect)
    {
        /* Arrange */
        $sut = new Data\DotKeyReader($values);

        /* Act */
        /* Assert */
        $this->assertSame($expect['value'], $sut($key));
        $this->assertSame($expect['value'], $sut[$key]);
    }

    /**
     * @group get
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_nonExistingKeyThrowsException()
    {
        /* Arrange */
        $sut = new Data\DotKeyReader($this->getValues());
        $key = 'not.existing.key';

        /* Act */
        /* Assert */
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage("undefined key {$key}");
        $sut($key);
    }

    /**
     * @group get
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_nonExistingKeyThrowsExceptionArrayAccess()
    {
        /* Arrange */
        $sut = new Data\DotKeyReader($this->getValues());
        $key = 'not.existing.key';

        /* Act */
        /* Assert */
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage("undefined key {$key}");
        $sut[$key];
    }

    /**
     * @group offsetExists
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_offsetExists()
    {
        /* Arrange */
        $sut = new Data\DotKeyReader($this->getValues());

        /* Act */
        /* Assert */
        $this->assertTrue(isset($sut['mailer.smtp.default.host']));
        $this->assertFalse(isset($sut['not.existing.key']));
    }

    /**
     * @group saftyGet
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_saftyGet()
    {
        /* Arrange */
        $isSafety = true;
        $sut = new Data\DotKeyReader($this->getValues(), $isSafety);
        $key = 'not.existing.key';

        /* Act */
        /* Assert */
        $this->assertNull($sut($key));
        $this->assertNull($sut[$key]);
        $this->assertSame('', $sut($key, ''));
        $this->assertSame('specified value', $sut($key, 'specified value'));
    }

    /**
     * @group offsetSet
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_offsetSet()
    {
        /* Arrange */
        $sut = new Data\DotKeyReader($this->getValues());

        /* Act */
        /* Assert */
        $this->expectException(\DomainException::class);
        $this->expectExceptionMessage('not supported');
        $sut['new.key'] = 'new value';
    }

    /**
     * @group offsetUnset
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_offsetUnset()
    {
        /* Arrange */
        $sut = new Data\DotKeyReader($this->getValues());

        /* Act */
        /* Assert */
        $this->expectException(\DomainException::class);
        $this->expectExceptionMessage('not supported');
        unset($sut['mailer.sender.default']);
    }

    private function getValues(): array
    {
        return [
            'isProduction' => false,
            'determineRouteBeforeAppMiddleware' => true,
            'displayErrorDetails' => true,
            'addContentLengthHeader' => false,
            'jwt' => [
                'secret' => [
                    'entryAccountActivation' => 'test_secret',
                ],
                'algorithm' => 'HS512',
            ],
            'mailer' => [
                'sender' => [
                    'default' => 'info@event-infodesk.com',
                ],
                'smtp' => [
                    'default' => [
                        'host' => 'test.smtp.host',
                        'port' => 25,
                        'username' => 'test_user',
                        'password' => 'test_password',
                    ],
                ],
            ],
            'db' => [
                'driver' => 'pdo_pgsql',
                'host' => 'test_db_host',
                'port' => 5432,
                'dbname' => 'test_db_name',
                'users' => [
                    'test_db_user' => 'test_db_user_password',
                ],
            ],
            'entry-account' => [
                'activationExpiredHour' => 24,
            ],
            'some' => [
                'deep' => [
                    'object' => new \StdClass(),
                ],
            ],
        ];
    }

    public function decodeDataProvider()
    {
        $f = [
            'id.eventSeq' => 127,
            'division.code' => 'r02',
            'division.number' => 75,
            'division.year' => 2020,
            'entranceTerm.startAt' => '2019-11-20',
            'entranceTerm.endedAt' => '2020-04-30',
            'sections.0.type' => 'typeA',
            'sections.0.entryTerm.startAt' => '2020-02-01',
            'sections.0.entryTerm.endedAt' => '2020-03-01',
            'sections.1.type' => 'typeB',
            'sections.1.entryTerm.startAt' => '2020-01-15',
            'sections.1.entryTerm.endedAt' => '2020-02-10',
            'allowedStagePerfs.0.areaType' => 'east',
            'allowedStagePerfs.0.term.startAt' => '2019-10-01',
            'allowedStagePerfs.0.term.endedAt' => '2019-12-01',
            'allowedStagePerfs.1.areaType' => 'west',
            'allowedStagePerfs.1.term.startAt' => '2019-10-15',
            'allowedStagePerfs.1.term.endedAt' => '2019-12-05',
            'overviewDueDate' => '2021-01-01',
            'createdAt' => (string)Chronos::now()->modify('-2days'),
            'modifiedAt' => (string)Chronos::now()->modify('-3days'),
        ];

        yield [
            'd' => [
                'f' => $f,
                'expect' => [
                    'event' => [
                        'id' => [
                            'eventSeq' => $f['id.eventSeq'],
                        ],
                        'division' => [
                            'code' => $f['division.code'],
                            'number' => $f['division.number'],
                            'year' => $f['division.year'],
                        ],
                        'entranceTerm' => [
                            'startAt' => $f['entranceTerm.startAt'],
                            'endedAt' => $f['entranceTerm.endedAt'],
                        ],
                        'sections' => [
                            [
                                'type' => $f['sections.0.type'],
                                'entryTerm' => [
                                    'startAt' => $f['sections.0.entryTerm.startAt'],
                                    'endedAt' => $f['sections.0.entryTerm.endedAt'],
                                ],
                            ],
                            [
                                'type' => $f['sections.1.type'],
                                'entryTerm' => [
                                    'startAt' => $f['sections.1.entryTerm.startAt'],
                                    'endedAt' => $f['sections.1.entryTerm.endedAt'],
                                ],
                            ],
                        ],
                        'allowedStagePerfs' => [
                            [
                                'areaType' => $f['allowedStagePerfs.0.areaType'],
                                'term' => [
                                    'startAt' => $f['allowedStagePerfs.0.term.startAt'],
                                    'endedAt' => $f['allowedStagePerfs.0.term.endedAt'],
                                ],
                            ],
                            [
                                'areaType' => $f['allowedStagePerfs.1.areaType'],
                                'term' => [
                                    'startAt' => $f['allowedStagePerfs.1.term.startAt'],
                                    'endedAt' => $f['allowedStagePerfs.1.term.endedAt'],
                                ],
                            ],
                        ],
                        'overviewDueDate' => $f['overviewDueDate'],
                        'createdAt' => $f['createdAt'],
                        'modifiedAt' => $f['modifiedAt'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider decodeDataProvider
     * @group decode
     */
    public function test_decode(array $d)
    {
        /* Arrange */
        $d = new Data\DotKeyReader($d);

        /* Act */
        $decoded = Data\DotKeyReader::decode($d['f']);

        /* Assert */
        $this->assertSame($d['expect.event'], $decoded);
    }

}
