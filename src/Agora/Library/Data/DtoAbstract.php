<?php
namespace Agora\Library\Data;

/**
 * 抽象DTO
 */
class DtoAbstract
{

    /**
     * constructor
     * @param array $fields OPTIONAL フィールド名, 値のペアリスト
     * @param bool $recallSetter OPTIONAL セッターの再呼び出し
     * @return void
     */
    public function __construct(array $fields = [], bool $recallSetter = false)
    {
        foreach ($fields as $fieldName => $value) {
            // 存在しないフィールドはスキップ
            if (!$this->fieldExists($fieldName)) continue;
            // setter実行
            $this->callSetter($fieldName, $value);
        }

        if ($recallSetter) {
            // 直接フィールドに設定された値全てをsetterを介して再設定する
            $fieldLister = (function () {
                $fieldName = $value = null;
                foreach (get_object_vars($this) as $fieldName => $value) {
                    yield $fieldName => $value;
                }
            });
            foreach ($fieldLister->call($this) as $fieldName => $value) {
                $this->callSetter($fieldName, $value);
            }
        }
    }

    /**
     * ファクトリ
     * @param array $fields フィールドと値のペアリスト
     * @return static
     */
    public static function create(array $fields = [])
    {
        return new static($fields);
    }

    /**
     * 不可視フィールド参照サポート
     * @param string $name フィールド名
     * @return mixed
     */
    public function __get($name)
    {
        $getterName = $this->createGetter($name);
        if (!$this->fieldExists($name) && !$this->hasGetter($getterName)) {
            $class = static::class;
            throw new \LogicException(
                "undefined "
                . "field {$class}::{$name} "
                . "or getter {$class}::{$getterName} "
                . "via property access");
        }
        return $this->callGetter($name);
    }

    /**
     * 未定義フィールドへの値設定サポート
     * @param string $fieldName フィールド名
     * @param mixed $value 値
     * @return void
     */
    public function __set(string $fieldName, $value)
    {
        if (!$this->fieldExists($fieldName)) {
            $class = static::class;
            throw new \LogicException(
                "undefined field {$class}::{$fieldName} via property setting");
        }
        $this->callSetter($fieldName, $value);
    }

    /**
     * 未定義メソッド実行サポート
     * @param string $name メソッド名
     * @param mixed[] $args メソッド引数リスト
     * @return mixed
     */
    public function __call(string $name, array $args)
    {
        $class = static::class;

        // getter処理
        if ($this->isGetter($name, $args, $fieldName)) {
            if (!$this->fieldExists($fieldName)) {
                throw new \LogicException(
                    "undefined field {$class}::{$fieldName} via getter");
            }

            return $this->getFieldValue($fieldName);
        }

        // setter処理
        if ($this->isSetter($name, $args, $fieldName, $value)) {
            if (!$this->fieldExists($fieldName)) {
                throw new \LogicException(
                    "undefined field {$class}::{$fieldName} via setter");
            }

            return $this->setFieldValue($fieldName, $value);
        }

        throw new \BadMethodCallException("undefined method {$class}::{$name}()");
    }

    /**
     * 複数プロパティに一括で値を設定する
     * @param array $values
     *  [{name: string, value: mixed}, ...]
     * @return static
     */
    public function setProperties(array $values)
    {
        foreach ($values as $name => $value) {
            $this->setProperty($name, $value);
        }
        return $this;
    }

    /**
     * プロパティに値を設定する
     * @param string $fieldName フィールド名
     * @param mixed $value 値
     * @return static
     */
    public function setProperty(string $fieldName, $value)
    {
        $this->__set($fieldName, $value);
        return $this;
    }

    /**
     * getterメソッド有無
     * @param string $getterName getter名
     * @return bool
     */
    private function hasGetter(string $getterName): bool
    {
        return (function ($getterName) {
            return method_exists($this, $getterName);
        })->call($this, $getterName);
    }

    /**
     * フィールド名からゲッターを生成する
     * @param strting $fieldName フィールド名
     * @return string
     */
    private function createGetter(string $fieldName): string
    {
        return 'get' . ucfirst($fieldName);
    }

    /**
     * getterを実行する
     * @param string $fieldName フィールド名
     * @return mixed
     */
    private function callGetter(string $fieldName)
    {
        return $this->{$this->createGetter($fieldName)}();
    }

    /**
     * setterを実行する
     * @param string $fieldName フィールド名
     * @param mixed $value 値
     * @return static
     */
    private function callSetter(string $fieldName, $value)
    {
        $setter = 'set' . ucfirst($fieldName);
        return $this->{$setter}($value);
    }

    /**
     * Getter判定
     * @param string $name メソッド名
     * @param mixed[] $args 引数リスト
     * @param string*|null &$fieldName フィールド名を受け取る参照引数
     * @return bool
     */
    private function isGetter(string $name, array $args, ?string &$fieldName): bool
    {
        // getで始まり引数がなければgetterとする
        if (preg_match('/^get(.+)$/', $name, $matches) > 0 && count($args) == 0) {
            $fieldName = lcfirst($matches[1]);
            return true;
        }
        return false;
    }

    /**
     * Setter判定
     * @param string $name メソッド名
     * @param mixed[] $args 引数リスト
     * @param string* &$fieldName 対応するフィールド名が設定される参照引数
     * @param mixed* &$value setter引数が設定される参照引数
     * @return bool
     */
    private function isSetter(
        string $name, array $args, ?string &$fieldName, &$value): bool
    {
        // setで始まり引数が1つであればsetterとする
        if (preg_match('/^set(.+)$/', $name, $matches) > 0 && count($args) == 1) {
            $fieldName = lcfirst($matches[1]);
            $value = reset($args);
            return true;
        }
        return false;
    }

    /**
     * フィールド存在判定
     * @param string $fieldName フィールド名
     * @return bool
     */
    private function fieldExists(string $fieldName): bool
    {
        return (function ($fieldName) {
            return property_exists($this, $fieldName);
        })->call($this, $fieldName);
    }

    /**
     * フィールドの値を取得する
     * @param string $fieldName フィールド名
     * @return mixed
     */
    private function getFieldValue(string $fieldName)
    {
        return (function ($fieldName) {
            return $this->{$fieldName};
        })->call($this, $fieldName);
    }

    /**
     * フィールドに値を設定する
     * @param string $fields フィールド名
     * @param mixed $value 値
     * @return static
     */
    private function setFieldValue(string $fieldName, $value)
    {
        return (function ($fieldName, $value) {
            $this->{$fieldName} = $value;
            return $this;
        })->call($this, $fieldName, $value);
    }

}
