<?php
namespace Agora\Library\Adapter\Data;

use Agora\Library\Data;
use Doctrine\DBAL;

/**
 * DBALクエリビルダページャー
 */
class DbalQueryBuilderPager
    implements Data\IPager
{

    /** @var int ページングしない */
    public const NO_PAGING = -1;

    /** @var int 最小ページ番号 */
    private const MIN_PAGE_NUMBER = 1;

    /** @var DBAL\Query\QueryBuilder $queryBuilder DBALクエリビルダ */
    private $queryBuilder = null;

    /** @var int ページ毎の件数 */
    private $countPerPage = self::NO_PAGING;

    /** @var int 現在のページ番号 */
    private $currentPageNumber = self::MIN_PAGE_NUMBER;

    /** @var string DTOクラス完全修飾名 */
    private $dtoName = '';

    /** @var int|null キャッシュされた件数 */
    private $cachedCount = null;

    /**
     * constructor
     * @param DBAL\Query\QueryBuilder $queryBuilder DBALクエリビルダ
     * @param string $dtoName DTOクラス完全修飾名
     * @return void
     */
    public function __construct(
        DBAL\Query\QueryBuilder $queryBuilder, string $dtoName)
    {
        $this->queryBuilder = $queryBuilder;
        $this->dtoName = $dtoName;
        $this->fetchArguments = [
            DBAL\FetchMode::CUSTOM_OBJECT,
            $dtoName, [[], true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getIterator(): \Traversable
    {
        return $this->getCurrentPage()->getIterator();
    }

    /**
     * @inheritdoc
     */
    public function getCount(): int
    {
        if (is_null($this->cachedCount)) {
            $this->cachedCount = $this->queryBuilder->getConnection()
                ->createQueryBuilder()
                ->from("({$this->queryBuilder->getSQL()})", "counting")
                ->select(['count(1) as count'])
                ->setParameters($this->queryBuilder->getParameters())
                ->execute()->fetchColumn();
            $this->queryBuilder = clone $this->queryBuilder;
        }

        return $this->cachedCount;
    }

    /**
     * @inheritdoc
     */
    public function getPageNumbers(): array
    {
        return range(self::MIN_PAGE_NUMBER, $this->getPageCount());
    }

    /**
     * @inheritdoc
     */
    public function getPageCount(): int
    {
        // ページングがなければ1ページ
        if ($this->countPerPage == self::NO_PAGING) return 1;

        // ページ毎の件数からページ数を算出
        $pageCount = (int)ceil($this->getCount() / $this->countPerPage);
        return $pageCount == 0 ? 1 : $pageCount;
    }

    /**
     * @inheritdoc
     */
    public function getCountPerPage(): int
    {
        return $this->countPerPage;
    }

    /**
     * @inheritdoc
     */
    public function getCurrentPageNumber(): int
    {
        return $this->currentPageNumber;
    }

    /**
     * @inheritdoc
     */
    public function getCurrentPage(): Data\IDtoList
    {
        $queryBuilder = clone $this->queryBuilder;
        if ($this->countPerPage != self::NO_PAGING) {
            $queryBuilder->setMaxResults($this->countPerPage)
                ->setFirstResult($this->countPerPage * ($this->currentPageNumber - 1));
        }
        return new DbalStatementDtoList(
            $queryBuilder->execute(), $this->dtoName);
    }

    /**
     * @inheritdoc
     */
    public function setCountPerPage(int $count): Data\IPager
    {
        $this->countPerPage = $count;
        return $this->setCurrentPageNumber($this->currentPageNumber);
    }

    /**
     * @inheritdoc
     */
    public function setCurrentPageNumber(int $number): Data\IPager
    {
        // 最小ページ番号調整
        $number = self::MIN_PAGE_NUMBER > $number ? self::MIN_PAGE_NUMBER : $number;

        // 最大ページ番号調整
        $maxPageCount = $this->getPageCount();
        $number = $number > $maxPageCount ? $maxPageCount : $number;

        $this->currentPageNumber = $number;
        return $this;
    }

}
