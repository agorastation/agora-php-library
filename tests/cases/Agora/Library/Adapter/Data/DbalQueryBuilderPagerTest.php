<?php
namespace Tests\Cases\Agora\Library\Adapter\Data;

use Agora\Library\Adapter\Data;
use Tests\Classes\Agora\Library\Data\TestDto;

/**
 * DBALクエリビルダページャー ユニットテスト
 * @coversDefaultClass \Agora\Library\Adapter\Data\DbalQueryBuilderPager
 * @see Data\DbalQueryBuilderPager
 */
final class DbalQueryBuilderPagerTest
    extends \Tests\Cases\DbalDbTestAbstract
{

    public function setUp(): void
    {
        parent::setUp();
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    public function getIteratorDataProvider()
    {
        yield 'I' => [
            'countPerfPage' => 3,
            'currentPageNumber' => 2,
            'expect' => [
                'currentPageNumber' => 2,
                'currentItems' => [
                    ['id' => 4, 'name' => 'テスト4'],
                    ['id' => 5, 'name' => 'テスト5'],
                    ['id' => 6, 'name' => 'テスト6'],
                ],
                'itemCountOnCurrentPage' => 3,
                'pageNumbers' => [1, 2, 3],
            ],
        ];

        yield 'II' => [
            'countPerfPage' => 3,
            'currentPageNumber' => 3,
            'expect' => [
                'currentPageNumber' => 3,
                'currentItems' => [
                    ['id' => 7, 'name' => 'テスト7'],
                    ['id' => 8, 'name' => 'テスト8'],
                ],
                'itemCountOnCurrentPage' => 2,
                'pageNumbers' => [1, 2, 3],
            ],
        ];

        yield 'III' => [
            'countPerfPage' => 3,
            'currentPageNumber' => 4,
            'expect' => [
                'currentPageNumber' => 3,
                'currentItems' => [
                    ['id' => 7, 'name' => 'テスト7'],
                    ['id' => 8, 'name' => 'テスト8'],
                ],
                'itemCountOnCurrentPage' => 2,
                'pageNumbers' => [1, 2, 3],
            ],
        ];

        yield 'IV' => [
            'countPerfPage' => 3,
            'currentPageNumber' => -1,
            'expect' => [
                'currentPageNumber' => 1,
                'currentItems' => [
                    ['id' => 1, 'name' => 'テスト1'],
                    ['id' => 2, 'name' => 'テスト2'],
                    ['id' => 3, 'name' => 'テスト3'],
                ],
                'itemCountOnCurrentPage' => 3,
                'pageNumbers' => [1, 2, 3],
            ],
        ];

        yield 'nothing' => [
            'countPerfPage' => 3,
            'currentPageNumber' => 2,
            'expect' => [
                'currentPageNumber' => 1,
                'currentItems' => [],
                'itemCountOnCurrentPage' => 0,
                'pageNumbers' => [1],
                'count' => 0,
            ],
            'where' => 'id = -1',
        ];

    }

    /**
     * @dataProvider getIteratorDataProvider
     * @group getIterator
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_getIterator(
        $countPerfPage, $currentPageNumber, $expect, $where = null)
    {
        /* Arrange */
        $queryBuilder = self::$adapter->createQueryBuilder()
            ->from('t_test')
            ->select(['id', 'name']);

        if (!is_null($where)) {
            $queryBuilder->where($where);
        }

        /* Act */
        $pager = (new Data\DbalQueryBuilderPager($queryBuilder, TestDto::class))
            ->setCountPerPage($countPerfPage)
            ->setCurrentPageNumber($currentPageNumber);

        /* Assert */
        $this->assertSame($countPerfPage, $pager->getCountPerPage());
        $this->assertSame($expect['currentPageNumber'], $pager->getCurrentPageNumber());
        $this->assertSame($expect['count'] ?? 8, $pager->getCount());
        $this->assertSame(
            $expect['itemCountOnCurrentPage'], $pager->getCurrentPage()->getCount());
        $this->assertSame($expect['pageNumbers'], $pager->getPageNumbers());
        foreach ($pager as $index => $item) {
            $this->assertEquals(TestDto::create($expect['currentItems'][$index]), $item);
        }
    }

    /**
     * @group setCountPerPage
     */
    public function test_setCountPerPage()
    {
        /* Arrange */
        $queryBuilder = self::$adapter->createQueryBuilder()
            ->from('t_test')
            ->select(['id', 'name']);

        $pager = (new Data\DbalQueryBuilderPager($queryBuilder, TestDto::class))
            ->setCountPerPage(2)
            ->setCurrentPageNumber(4);

        /* Act */
        $pager->setCountPerPage(5);

        /* Assert */
        $this->assertSame(5, $pager->getCountPerPage());
        $this->assertSame(2, $pager->getCurrentPageNumber());
    }

    /**
     * @group noPaging
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_noPaging()
    {
        /* Arrange */
        $queryBuilder = self::$adapter->createQueryBuilder()
            ->from('t_test')
            ->select(['id', 'name']);

        /* Act */
        $pager = (new Data\DbalQueryBuilderPager($queryBuilder, TestDto::class));

        /* Assert */
        $expect = [
            ['id' => 1, 'name' => 'テスト1'],
            ['id' => 2, 'name' => 'テスト2'],
            ['id' => 3, 'name' => 'テスト3'],
            ['id' => 4, 'name' => 'テスト4'],
            ['id' => 5, 'name' => 'テスト5'],
            ['id' => 6, 'name' => 'テスト6'],
            ['id' => 7, 'name' => 'テスト7'],
            ['id' => 8, 'name' => 'テスト8'],
        ];
        foreach ($pager as $index => $item) {
            $this->assertEquals(TestDto::create($expect[$index]), $item);
        }

        $this->assertSame($pager::NO_PAGING, $pager->getCountPerPage());
        $this->assertSame(1, $pager->getCurrentPageNumber());
        $this->assertSame(8, $pager->getCount());
        $this->assertSame(8, $pager->getCurrentPage()->getCount());
        $this->assertSame([1], $pager->getPageNumbers());
    }
}
