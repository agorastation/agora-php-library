<?php
namespace tests\classes\Agora\Library\Factory;

use Agora\Library\Factory;

/**
 * 抽象テストファクトリ
 */
class TestFactoryAbstract
{

    use Factory\SimpleInstanceCachingTrait;

    public function create(...$args)
    {
        return $this->createInstance(...$args);
    }

}
