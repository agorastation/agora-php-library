<?php
namespace Tests\Cases\Agora\Tests;

use Agora\Tests;
use Doctrine\DBAL;

/**
 * DBAL-DBユニットテストトレイトユニットテスト
 * @coversDefaultClass \Agora\Tests\DbalDbTestTrait
 * @see Tests\DbalDbTestTrait
 */
final class DbalDbTestTraitTest
    extends \Tests\Cases\TestAbstract
{

    private $sut;

    public function setUp(): void
    {
        parent::setUp();
        $this->sut = $this->getMockForTrait(Tests\DbalDbTestTrait::class);
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @group createDbAdapter
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_createDbAdapter()
    {
        /* Arrange */

        /* Act */
        $adapter = (function () {
            return self::createDbAdapter(['driver' => 'pdo_sqlite', 'dbname' => ':memory']);
        })->call($this->sut);

        /* Assert */
        $this->assertInstanceOf(DBAL\Driver\Connection::class, $adapter);
        return $adapter;
    }

    /**
     * @depends test_createDbAdapter
     * @group initializeDb
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_initializeDb(DBAL\Driver\Connection $adapter)
    {
        /* Arrange */
        /* Act */
        (function ($adapter, $projectRoot) {
            $this->initializeDb($adapter, "{$projectRoot}tests/data/initial/once");
            $this->initializeDb($adapter, "{$projectRoot}tests/data/initial/each");
        })->call($this->sut, $adapter, $this->projectRoot);

        /* Assert */
        $row = $adapter->query('select id, name from t_test where id = 3')->fetch();
        $this->assertSame('3', $row['id']);
        $this->assertSame('テスト3', $row['name']);
    }

}
