<?php
namespace Tests\Cases\Agora\Library\Data;

use \Tests\Classes\Agora\Library\Data\TestDto;

/**
 * 抽象DTO ユニットテスト
 * @coversDefaultClass \Agora\Library\Data\DtoAbstract
 * @see \Agora\Library\Data\DtoAbstract
 */
final class DtoAbstractTest
    extends \Tests\Cases\TestAbstract
{

    public function setUp(): void
    {
        parent::setUp();
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    public function commonDataProvider()
    {
        yield 'common' => [
            'id' => 5,
            'name' => 'test name',
        ];

    }

    /**
     * @dataProvider commonDataProvider
     * @group create
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_create($id, $name)
    {
        /* Arrange */
        $fields = [
            'id' => $id,
            'name' => " {$name} ",
        ];

        /* Act */
        $dto = TestDto::create($fields);

        /* Assert */
        $this->assertSame($id, $dto->id);
        $this->assertSame($name, $dto->name);
        $this->assertSame("{$id},{$name}", $dto->csv);
    }

    /**
     * @dataProvider commonDataProvider
     * @group nonExistingFieldsIgnore
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_nonExistingFieldsIgnore($id, $name)
    {
        /* Arrange */
        $fields = [
            'id' => $id,
            'name' => " {$name} ",
            'otherFields' => 'other fields',
        ];

        /* Act */
        $dto = TestDto::create($fields);

        /* Assert */
        $this->assertSame($id, $dto->id);
        $this->assertSame($name, $dto->name);
    }

    /**
     * @dataProvider commonDataProvider
     * @group recallSetter
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_recallSetter($id, $name)
    {
        /* Arrange */
        $dto = TestDto::create();
        (function ($id, $name) {
            $this->id = $id;
            $this->name = " {$name} ";
        })->call($dto, $id, $name);

        $this->assertSame(" {$name} ", $dto->name);

        /* Act */
        $dto->__construct([], true);

        /* Assert */
        $this->assertSame($id, $dto->id);
        $this->assertSame($name, $dto->name);
    }

    /**
     * @dataProvider commonDataProvider
     * @group virtualSetter
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_virtualSetter($id, $name)
    {
        /* Arrange */
        $dto = TestDto::create();

        /* Act */
        $self = $dto->setId($id)
            ->setName(" {$name} ");

        /* Assert */
        $this->assertSame($self, $dto);
        $this->assertSame($id, $dto->id);
        $this->assertSame($name, $dto->name);
    }

    /**
     * @dataProvider commonDataProvider
     * @group virtualProperty
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_virtualProperty($id, $name)
    {
        /* Arrange */
        $dto = TestDto::create();

        /* Act */
        $dto->id = $id;
        $dto->name = " {$name} ";

        /* Assert */
        $this->assertSame($id, $dto->id);
        $this->assertSame($name, $dto->name);
    }

    /**
     * @group undefinedFieldGetting
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_undefinedFieldGetting()
    {
        /* Arrange */
        $dto = TestDto::create();

        /* Act */
        /* Assert */
        $class = get_class($dto);
        $this->expectException(\LogicException::class);
        $this->expectExceptionMessage(
            "undefined "
            . "field {$class}::undefinedField "
            . "or getter {$class}::getUndefinedField "
            . "via property access");
        $dto->undefinedField;
    }

    /**
     * @group undefinedFieldSetting
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_undefinedFieldSetting()
    {
        /* Arrange */
        $dto = TestDto::create();

        /* Act */
        /* Assert */
        $class = get_class($dto);
        $this->expectException(\LogicException::class);
        $this->expectExceptionMessage(
            "undefined field {$class}::undefinedField via property setting");

        $dto->undefinedField = 'undefined field\'s value';
    }

    /**
     * @group getterOnUndefinedField
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_getterOnUndefinedField()
    {
        /* Arrange */
        $dto = TestDto::create();

        /* Act */
        /* Assert */
        $class = get_class($dto);
        $this->expectException(\LogicException::class);
        $this->expectExceptionMessage(
            "undefined field {$class}::undefinedField via getter");
        $dto->getUndefinedField();
    }

    /**
     * @group setterOnUndefinedField
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_setterOnUndefinedField()
    {
        /* Arrange */
        $dto = TestDto::create();

        /* Act */
        /* Assert */
        $class = get_class($dto);
        $this->expectException(\LogicException::class);
        $this->expectExceptionMessage(
            "undefined field {$class}::undefinedField via setter");
        $dto->setUndefinedField('undefined field value');
    }

    /**
     * @group undefinedMethod
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_undefinedMethod()
    {
        /* Arrange */
        $dto = TestDto::create();

        /* Act */
        /* Assert */
        $class = get_class($dto);
        $this->expectException(\BadMethodCallException::class);
        $this->expectExceptionMessage(
            "undefined method {$class}::doSomething()");
        $dto->doSomething();
    }

    public function propertyDataProvider()
    {
        yield [
            'values' => [
                'id' => 3,
                'name' => 'test',
            ],
        ];

    }

    /**
     * @dataProvider propertyDataProvider
     * @group setProperty
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_setProperty(array $values)
    {
        /* Arrange */
        /* Act */
        $dto = TestDto::create()
            ->setProperty('id', $values['id'])
            ->setProperty('name', " {$values['name']} ");

        /* Assert */
        $this->assertSame($values['id'], $dto->id);
        $this->assertSame($values['name'], $dto->name);
    }

    /**
     * @dataProvider propertyDataProvider
     * @group setProperties
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_setProperties(array $values)
    {
        /* Arrange */
        /* Act */
        $dto = TestDto::create()
            ->setProperties([
                'id' => $values['id'],
                'name' => " {$values['name']} "
            ]);

        /* Assert */
        $this->assertSame($values['id'], $dto->id);
        $this->assertSame($values['name'], $dto->name);
    }

}
