# Agora Core Library

## 課題

* 全般
* DbalQueryBuilderPager
    * 公開メソッドが多すぎる
* DbalStatementDtoList
    * ステートメントのカーソルがスクロール可能にできないとパフォーマンスに影響がでる
      現状は fetchAll()の結果をキャッシングしている
        * スクローラブルカーソルはステートメント生成時のprepare()でしか指定できない
          且つ、Wrapperコネクション/クエリビルダでは設定項目を受け付けていない
            * 対応として、もう一段ラップするなりデコレートするなりし
              内部でクロージャによるスコープ変更を利用して
              オリジナルのステートメントの状態を変更する方法もあるが、
              パフォーマンスがあまり良くないので本末転倒感がある

## クラス図

@startuml
scale max 1024 width
skinparam classAttributeIconSize 0
hide empty members

namespace Native {

    interface IteratorAggregate {
        + getIterator(): Traversable
    }

}

namespace Agora.Library.Adapter {

    Agora.Library.Data.IDtoList <|.. DbalStatementDtoList
    DbalStatementDtoList *--> "*" Agora.Library.Data.DtoAbstract
    class DbalStatementDtoList {
    }

    Agora.Library.Data.IPager <|.. DbalQueryBuilderPager
    DbalQueryBuilderPager *--> "*" Agora.Library.Data.IDtoList
    class DbalQueryBuilderPager {
    }

}

namespace Agora.Library.Data {

    Native.IteratorAggregate <|-- IDtoList
    IDtoList *--> "*" DtoAbstract
    interface IDtoList {
        + getCount(): int
    }

    Native.IteratorAggregate <|-- IPager
    IPager *--> "*" DtoAbstract
    interface IPager {
        + getCount(): int
        + getPageNumbers(): int[]
        + getPageCount(): int
        + getCountPerPage(): int
        + getCurrentPageNumber(): int
        + getCurrentPage(): iterable

        + setCountPerPage(count: int): static
        + setCurrentPageNumber(number: int): static
    }

    abstract class DtoAbstract {

        + {static} create(fields: IDictionary<string, mixed>): static

        + __get(name: string): mixed
        + __set(field: string, value: mixed): void
        + __call(name: string, args: mixed[]): mixed

    }

}

@enduml
